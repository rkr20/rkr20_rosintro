#!/usr/bin/bash

rosservice call reset
rosservice call /turtle1/teleport_absolute 2  5  1.6
rosservice call clear
rosservice call turtle1/set_pen 255 255 0 10 off
rosservice call spawn 7 5 1.6 turtle2
rosservice call turtle2/set_pen 255 0 255 10 off
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist  '[4.0,0.0,0.0]' '[0.0,0.0,0.0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist  '[2.0,0.0,0.0]' '[0.0,0.0,0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist  '[7.0,0.0,0.0]' '[0.0,0.0,-5.5]'
rosservice call turtle2/set_pen 255 255 0 10 on
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist  '[1.0,0.25,0.0]' '[0.0,0.0,0.0]'
rosservice call turtle2/set_pen 255 0 255 10 off
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist  '[-4.0,0.0,0.0]' '[0.0,0.0,0.0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist  '[2.5,-0.5,0.0]' '[0.0,0.0,-10.0]'
rosservice call turtle1/set_pen 255 255 0 10 on
rosservice call turtle2/set_pen 255 0 255 10 on
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist  '[-5.0,0.0,0.0]' '[0.0,0.0,0.0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist  '[5.0,0.0,0.0]' '[0.0,0.0,0.0]'






