#!/usr/bin/bash

rosservice call reset
rosservice call /turtle1/teleport_absolute 2  5  1.6
rosservice call clear
rosservice call turtle1/set_pen 255 255 0 10 off
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist  '[4.0,0.0,0.0]' '[0.0,0.0,0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist  '[7.0,0.0,0.0]' '[0.0,0.0,-5.5]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist  '[-4.0,0.0,0.0]' '[0.0,0.0,0.0]'

