#!/usr/bin/env python
from __future__ import print_function
from ctypes.wintypes import WPARAM
from six.moves import input

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
try:
    from math import pi, tau, dist, fabs, cos
except:  # For Python 2 compatibility
    from math import pi, fabs, cos, sqrt

    tau = 2.0 * pi
from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

class MoveGroupPythonInterface(object):
    initial_joint_goal = None
    def __init__(self):
        super(MoveGroupPythonInterface, self).__init__()
        moveit_commander.roscpp_initialize(sys.argv)
        rospy.init_node("move_group_python_interface_tutorial", anonymous=True)
        robot = moveit_commander.RobotCommander()
        scene = moveit_commander.PlanningSceneInterface()
        group_name = "manipulator"
        move_group = moveit_commander.MoveGroupCommander(group_name)
        display_trajectory_publisher = rospy.Publisher(
            "/move_group/display_planned_path",
            moveit_msgs.msg.DisplayTrajectory,
            queue_size=20,
        )
        planning_frame = move_group.get_planning_frame()
        print("============ Planning frame: %s" % planning_frame)
        # We can also print the name of the end-effector link for this group:
        eef_link = move_group.get_end_effector_link()
        print("============ End effector link: %s" % eef_link)
        # We can get a list of all the groups in the robot:
        group_names = robot.get_group_names()
        print("============ Available Planning Groups:", robot.get_group_names())
        # Sometimes for debugging it is useful to print the entire state of the
        # robot:
        print("============ Printing robot state")
        print(robot.get_current_state())
        print("")
        # Misc variables
        self.box_name = ""
        self.robot = robot
        self.scene = scene
        self.move_group = move_group
        self.display_trajectory_publisher = display_trajectory_publisher
        self.planning_frame = planning_frame
        self.eef_link = eef_link
        self.group_names = group_names

    def initial_position(self):

        #this function is used to move the robot to the initial position
        #it doesnt actually move the robot, it just sets the initial position
        #help it avoid future singularity issues when setting the first waypoint
      
        move_group = self.move_group
        joint_goal = move_group.get_current_joint_values()
        initial_joint_goal = joint_goal
        move_group.go(joint_goal, wait=True)
        move_group.stop()

        pose_goal = geometry_msgs.msg.Pose()
        waypoints = []
        wpose = pose_goal
        wpose.orientation.w = 1.0 
        wpose.position.x = 0.2
        wpose.position.y = 0.2
        wpose.position.z = 0.3

        waypoints.append(copy.deepcopy(wpose))
        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0
        )


        move_group.set_pose_target(pose_goal)
        success = move_group.go(wait=True)
        move_group.stop()
        move_group.clear_pose_targets()
        current_pose = self.move_group.get_current_pose().pose
        current_joints = move_group.get_current_joint_values()

        #print the current joint values
        print("Current joint values: ", current_joints)
        
        return success
    
    def draw_R(self):
        move_group = self.move_group
        #this function is used to draw the letter R
        pose_goal = geometry_msgs.msg.Pose()
        waypoints = []
        wpose = pose_goal
        wpose.orientation.w = 1.0 
        wpose.position.x = 0.2 #creating the "I" shape of the letter R
        wpose.position.y = 0.2
        wpose.position.z = 0.3

        waypoints.append(copy.deepcopy(wpose))
        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0
        )


        move_group.set_pose_target(pose_goal)
        success = move_group.go(wait=True)
        move_group.stop()
        move_group.clear_pose_targets()
        current_pose = self.move_group.get_current_pose().pose

        pose_goal = geometry_msgs.msg.Pose()
        waypoints = []
        wpose = pose_goal
        wpose.orientation.w = 1.0 
        wpose.position.x = 0.4 #once reached here, the back of the R is drawn
        wpose.position.y = 0.4
        wpose.position.z = 0.3

        waypoints.append(copy.deepcopy(wpose))
        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0
        )
        move_group.execute(plan, wait=True)
        success = move_group.go(wait=True)
        move_group.stop()
        current_pose = move_group.get_current_pose().pose

        pose_goal = geometry_msgs.msg.Pose()
        waypoints = []
        wpose = pose_goal
        wpose.orientation.w = 1.0 
        wpose.position.x = 0.43 #the top of the R is drawn
        wpose.position.y = 0.38
        wpose.position.z = 0.3

        waypoints.append(copy.deepcopy(wpose))
        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0
        )
        move_group.execute(plan, wait=True)
        success = move_group.go(wait=True)
        move_group.stop()
        current_pose = move_group.get_current_pose().pose

        pose_goal = geometry_msgs.msg.Pose()
        waypoints = []
        wpose = pose_goal
        wpose.orientation.w = 1.0 
        wpose.position.x = 0.45 #the curve of the R is being drawn
        wpose.position.y = 0.3
        wpose.position.z = 0.3

        waypoints.append(copy.deepcopy(wpose))
        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0
        )
        move_group.execute(plan, wait=True)
        success = move_group.go(wait=True)
        move_group.stop()
        current_pose = move_group.get_current_pose().pose
        
        pose_goal = geometry_msgs.msg.Pose()
        waypoints = []
        wpose = pose_goal
        wpose.orientation.w = 1.0 
        wpose.position.x = 0.40 #the curve of the R is being drawn
        wpose.position.y = 0.26
        wpose.position.z = 0.3

        waypoints.append(copy.deepcopy(wpose))
        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0
        )
        move_group.execute(plan, wait=True)
        success = move_group.go(wait=True)
        move_group.stop()
        current_pose = move_group.get_current_pose().pose

        pose_goal = geometry_msgs.msg.Pose()
        waypoints = []
        wpose = pose_goal
        wpose.orientation.w = 1.0 
        wpose.position.x = 0.33 #the curve of the R is fnished, begin the leg of the R
        wpose.position.y = 0.3
        wpose.position.z = 0.3

        waypoints.append(copy.deepcopy(wpose))
        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0
        )
        move_group.execute(plan, wait=True)
        success = move_group.go(wait=True)
        move_group.stop()
        current_pose = move_group.get_current_pose().pose

        pose_goal = geometry_msgs.msg.Pose()
        waypoints = []
        wpose = pose_goal
        wpose.orientation.w = 1.0 
        wpose.position.x = 0.32 #leg of the R
        wpose.position.y = 0.3
        wpose.position.z = 0.3

        waypoints.append(copy.deepcopy(wpose))
        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0
        )
        move_group.execute(plan, wait=True)
        success = move_group.go(wait=True)
        move_group.stop()
        current_pose = move_group.get_current_pose().pose

        pose_goal = geometry_msgs.msg.Pose()
        waypoints = []
        wpose = pose_goal
        wpose.orientation.w = 1.0 
        wpose.position.x = 0.32 #waypoint to draw the leg of the R
        wpose.position.y = 0.2
        wpose.position.z = 0.3

        waypoints.append(copy.deepcopy(wpose))
        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0
        )
        move_group.execute(plan, wait=True)
        success = move_group.go(wait=True)
        move_group.stop()
        current_pose = move_group.get_current_pose().pose

        pose_goal = geometry_msgs.msg.Pose()
        waypoints = []
        wpose = pose_goal
        wpose.orientation.w = 1.0 
        wpose.position.x = 0.32 #finish the leg of the R
        wpose.position.y = 0.1
        wpose.position.z = 0.3

        waypoints.append(copy.deepcopy(wpose))
        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0
        )
        move_group.execute(plan, wait=True)
        success = move_group.go(wait=True)
        move_group.stop()
        current_pose = move_group.get_current_pose().pose
        
        #end of the R
        return success
    def draw_I(self):  

        #This function draws the letter I capital letter - 3 lines, 1 vertical and 2 horizontal  
        move_group = self.move_group

        #BEGIN INITIAL POSITION TO DRAW THE I FROM THE BOTTOM UP
        pose_goal = geometry_msgs.msg.Pose()
        waypoints = []
        wpose = pose_goal
        wpose.orientation.w = 1.0 
        wpose.position.x = 0.2
        wpose.position.y = 0.2
        wpose.position.z = 0.3

        waypoints.append(copy.deepcopy(wpose))
        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0
        )


        move_group.set_pose_target(pose_goal)
        success = move_group.go(wait=True)
        move_group.stop()
        move_group.clear_pose_targets()
        current_pose = self.move_group.get_current_pose().pose


        #BEGIN DRAWING THE I , First vertical line
        #done in the xy plane for ease of drawing, helps avoid collisions with the table and singularity issues

        pose_goal = geometry_msgs.msg.Pose()
        waypoints = []
        wpose = pose_goal
        wpose.orientation.w = 1.0 
        wpose.position.x = 0.4
        wpose.position.y = 0.4
        wpose.position.z = 0.3

        waypoints.append(copy.deepcopy(wpose))
        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0
        )
        move_group.set_pose_target(pose_goal)
        success = move_group.go(wait=True)
        move_group.stop()
        move_group.clear_pose_targets()
        current_pose = self.move_group.get_current_pose().pose


        #Once the robot reaches here, it draws the first horizontal line at the top of the I from center to right

        pose_goal = geometry_msgs.msg.Pose()
        waypoints = []
        wpose = pose_goal
        wpose.orientation.w = 1.0 
        wpose.position.x = 0.5
        wpose.position.y = 0.3
        wpose.position.z = 0.3

        waypoints.append(copy.deepcopy(wpose))
        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0
        )
        move_group.set_pose_target(pose_goal)
        success = move_group.go(wait=True)
        move_group.stop()
        move_group.clear_pose_targets()
        current_pose = self.move_group.get_current_pose().pose

        #Once the robot reaches here, continues to draw the horizontal line at the top of the I from left to right

        pose_goal = geometry_msgs.msg.Pose()
        waypoints = []
        wpose = pose_goal
        wpose.orientation.w = 1.0 
        wpose.position.x = 0.3
        wpose.position.y = 0.5
        wpose.position.z = 0.3

        waypoints.append(copy.deepcopy(wpose))
        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0
        )
        move_group.set_pose_target(pose_goal)
        success = move_group.go(wait=True)
        move_group.stop()
        move_group.clear_pose_targets()
        current_pose = self.move_group.get_current_pose().pose

        #Once the robot reaches here, continues to draw the horizontal line at the top of the I from left to center

        pose_goal = geometry_msgs.msg.Pose()
        waypoints = []
        wpose = pose_goal
        wpose.orientation.w = 1.0 
        wpose.position.x = 0.4
        wpose.position.y = 0.4
        wpose.position.z = 0.3

        waypoints.append(copy.deepcopy(wpose))
        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0
        )
        move_group.set_pose_target(pose_goal)
        success = move_group.go(wait=True)
        move_group.stop()
        move_group.clear_pose_targets()
        current_pose = self.move_group.get_current_pose().pose

        #Once the robot reaches here, it draws back to the initial position to draw the bottom of the I 

        pose_goal = geometry_msgs.msg.Pose()
        waypoints = []
        wpose = pose_goal
        wpose.orientation.w = 1.0 
        wpose.position.x = 0.25
        wpose.position.y = 0.25
        wpose.position.z = 0.3

        waypoints.append(copy.deepcopy(wpose))
        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0
        )
        move_group.set_pose_target(pose_goal)
        success = move_group.go(wait=True)
        move_group.stop()
        move_group.clear_pose_targets()
        current_pose = self.move_group.get_current_pose().pose

        #Once the robot reaches here, it draws the bottom of the I from center to right
        pose_goal = geometry_msgs.msg.Pose()
        waypoints = []
        wpose = pose_goal
        wpose.orientation.w = 1.0 
        wpose.position.x = 0.35
        wpose.position.y = 0.15
        wpose.position.z = 0.3

        waypoints.append(copy.deepcopy(wpose))
        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0
        )
        move_group.set_pose_target(pose_goal)
        success = move_group.go(wait=True)
        move_group.stop()
        move_group.clear_pose_targets()
        current_pose = self.move_group.get_current_pose().pose

        #Once the robot reaches here, it draws the bottom of the I from right to left
        pose_goal = geometry_msgs.msg.Pose()
        waypoints = []
        wpose = pose_goal
        wpose.orientation.w = 1.0 
        wpose.position.x = 0.15
        wpose.position.y = 0.35
        wpose.position.z = 0.3

        waypoints.append(copy.deepcopy(wpose))
        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0
        )
        move_group.set_pose_target(pose_goal)
        success = move_group.go(wait=True)
        move_group.stop()
        move_group.clear_pose_targets()
        current_pose = self.move_group.get_current_pose().pose

        #Once the robot reaches here, it draws the bottom of the I from left to center
        pose_goal = geometry_msgs.msg.Pose()
        waypoints = []
        wpose = pose_goal
        wpose.orientation.w = 1.0 
        wpose.position.x = 0.25
        wpose.position.y = 0.25
        wpose.position.z = 0.3

        waypoints.append(copy.deepcopy(wpose))
        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0
        )
        move_group.set_pose_target(pose_goal)
        success = move_group.go(wait=True)
        move_group.stop()
        move_group.clear_pose_targets()
        current_pose = self.move_group.get_current_pose().pose

        return success
    def draw_S(self):
        move_group = self.move_group
        #This function draws the S
        #First, it draws the top of the S from top right to bottom left
        pose_goal = geometry_msgs.msg.Pose()
        waypoints = []
        wpose = pose_goal
        wpose.orientation.w = 1.0 
        wpose.position.x = 0.45 #initial "horn" of the S
        wpose.position.y = 0.35
        wpose.position.z = 0.3
        waypoints.append(copy.deepcopy(wpose))
        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0
        )
        move_group.set_pose_target(pose_goal)
        success = move_group.go(wait=True)
        move_group.stop()
        move_group.clear_pose_targets()
        current_pose = self.move_group.get_current_pose().pose

        #print the current pose of the robot and joint values
        print (move_group.get_current_joint_values())


        pose_goal = geometry_msgs.msg.Pose()
        waypoints = []
        wpose = pose_goal
        wpose.orientation.w = 1.0 
        wpose.position.x = 0.4 #creating the first arc of the S
        wpose.position.y = 0.45
        wpose.position.z = 0.3
        waypoints.append(copy.deepcopy(wpose))
        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0
        )
        move_group.set_pose_target(pose_goal)
        success = move_group.go(wait=True)
        move_group.stop()
        move_group.clear_pose_targets()
        current_pose = self.move_group.get_current_pose().pose

        pose_goal = geometry_msgs.msg.Pose()
        waypoints = []
        wpose = pose_goal
        wpose.orientation.w = 1.0 
        wpose.position.x = 0.3 #creating the first arc of the S
        wpose.position.y = 0.45
        wpose.position.z = 0.3
        waypoints.append(copy.deepcopy(wpose))
        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0
        )
        move_group.set_pose_target(pose_goal)
        success = move_group.go(wait=True)
        move_group.stop()
        move_group.clear_pose_targets()
        current_pose = self.move_group.get_current_pose().pose

        pose_goal = geometry_msgs.msg.Pose()
        waypoints = []
        wpose = pose_goal
        wpose.orientation.w = 1.0 
        wpose.position.x = 0.28 #creating the first arc of the S
        wpose.position.y = 0.35
        wpose.position.z = 0.3
        waypoints.append(copy.deepcopy(wpose))
        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0
        )
        move_group.set_pose_target(pose_goal)
        success = move_group.go(wait=True)
        move_group.stop()
        move_group.clear_pose_targets()
        current_pose = self.move_group.get_current_pose().pose

        pose_goal = geometry_msgs.msg.Pose()
        waypoints = []
        wpose = pose_goal
        wpose.orientation.w = 1.0 
        wpose.position.x = 0.33 #creating the first arc of the S
        wpose.position.y = 0.25
        wpose.position.z = 0.3
        waypoints.append(copy.deepcopy(wpose))
        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0
        )
        move_group.set_pose_target(pose_goal)
        success = move_group.go(wait=True)
        move_group.stop()
        move_group.clear_pose_targets()
        current_pose = self.move_group.get_current_pose().pose

        pose_goal = geometry_msgs.msg.Pose()
        waypoints = []
        wpose = pose_goal
        wpose.orientation.w = 1.0 
        wpose.position.x = 0.35 #creating the second arc of the S
        wpose.position.y = 0.15
        wpose.position.z = 0.3
        waypoints.append(copy.deepcopy(wpose))
        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0
        )
        move_group.set_pose_target(pose_goal)
        success = move_group.go(wait=True)
        move_group.stop()
        move_group.clear_pose_targets()
        current_pose = self.move_group.get_current_pose().pose

        pose_goal = geometry_msgs.msg.Pose()
        waypoints = []
        wpose = pose_goal
        wpose.orientation.w = 1.0 
        wpose.position.x = 0.3 #creating the second arc of the S
        wpose.position.y = 0.1
        wpose.position.z = 0.3
        waypoints.append(copy.deepcopy(wpose))
        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0
        )
        move_group.set_pose_target(pose_goal)
        success = move_group.go(wait=True)
        move_group.stop()
        move_group.clear_pose_targets()
        current_pose = self.move_group.get_current_pose().pose

        pose_goal = geometry_msgs.msg.Pose()
        waypoints = []
        wpose = pose_goal
        wpose.orientation.w = 1.0 
        wpose.position.x = 0.2 #creating the second arc of the S
        wpose.position.y = 0.1
        wpose.position.z = 0.3
        waypoints.append(copy.deepcopy(wpose))
        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0
        )
        move_group.set_pose_target(pose_goal)
        success = move_group.go(wait=True)
        move_group.stop()
        move_group.clear_pose_targets()
        current_pose = self.move_group.get_current_pose().pose

        pose_goal = geometry_msgs.msg.Pose()
        waypoints = []
        wpose = pose_goal
        wpose.orientation.w = 1.0 
        wpose.position.x = 0.15 #creating the second arc of the S
        wpose.position.y = 0.15
        wpose.position.z = 0.3
        waypoints.append(copy.deepcopy(wpose))
        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0
        )
        move_group.set_pose_target(pose_goal)
        success = move_group.go(wait=True)
        move_group.stop()
        move_group.clear_pose_targets()
        current_pose = self.move_group.get_current_pose().pose

        pose_goal = geometry_msgs.msg.Pose()
        waypoints = []
        wpose = pose_goal
        wpose.orientation.w = 1.0 
        wpose.position.x = 0.15 #creating the second arc of the S
        wpose.position.y = 0.25
        wpose.position.z = 0.3
        waypoints.append(copy.deepcopy(wpose))
        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0
        )
        move_group.set_pose_target(pose_goal)
        success = move_group.go(wait=True)
        move_group.stop()
        move_group.clear_pose_targets()
        current_pose = self.move_group.get_current_pose().pose

        #end of the S
        return success





def main():
    try:
        print("")
        print("----------------------------------------------------------")
        print("Welcome to my Midterm Project")
        print("----------------------------------------------------------")
        ur5 = MoveGroupPythonInterface()

        #Begin the initial join state to get it in the XY Plane
        ur5.initial_position()

        print("----------------------------------------------------------")
        print("Drawing R")
        print("----------------------------------------------------------")
        ur5.draw_R()

        print("----------------------------------------------------------")
        print("Drawing I")
        print("----------------------------------------------------------")

        ur5.draw_I()

        print("----------------------------------------------------------")
        print("Drawing S")
        print("----------------------------------------------------------")
        ur5.draw_S()

        
        #Ris has been drawn, terminate the program
        print("----------------------------------------------------------")
        print("End of my Midterm Project")
        print("----------------------------------------------------------")
        print("")
    except rospy.ROSInterruptException:
        return
    except KeyboardInterrupt:
        return


if __name__ == "__main__":
    main()